var fs = require("fs");
var Buffer = require("buffer").Buffer;
var filename;
var stats;
var fileSizeInBytes;

if (typeof(process.argv[2]) === "undefined") {
    console.log("Use: node iml_logo_reader.js fontFilename.lgo");
    process.exit(1);
} else {
    filename = process.argv[2];
    stats = fs.statSync(filename);
    fileSizeInBytes = stats["size"];
}

function getHextetFrom(decimal) {
    var binary = decimal.toString(2);

    while (binary.length < 16) {
        binary = "0" + binary;
    }

    return binary.split("").reverse().map(function (x) {
        return x === "1";
    });
}

function Bitmap(nbCol, nbLine, sizeCol, buffer) {

    var bitmap = [];
    var tmpBitmap = [];


    for (var i = 0; i < buffer.length / 2; i++) {
        var hextet = getHextetFrom(buffer.readUInt16LE(i * 2));

        // Performance reason! Faster that run Array.concat per loop
        hextet.forEach(function (bit) {
            tmpBitmap.push(bit);
        });
    }

    // Prepare bitmap
    for (var y = nbLine - 1; y >= 0; y--) {
        var line = [];

        for (var x = 0; x < nbCol; x++) {
            line.push(tmpBitmap[x * sizeCol * 8 * 2 + y]);
        }
        bitmap.push(line);
    }

    return {
        width: nbCol,
        height: nbLine,
        bitmap: bitmap,
        display: function () {

            console.log("width:", this.width, "height:", this.height);

            for (var y = 0; y < this.height; y++) {
                console.log(
                    this.bitmap[y]
                        .map(function (element) {
                            return element === true ? "#" : " ";
                        })
                        .join("")
                );
            }
        }
    }
}

fs.open(filename, "r", function (status, fd) {
    if (status) {
        console.log(status.message);
        return;
    }

    var buffer = new Buffer(fileSizeInBytes, "binary");

    fs.read(fd, buffer, 0, buffer.length, 0, function (err, bytesRead) {

        // All offsets are static in this file format
        var version = buffer.toString("utf8", 0, 16);
        var nbCol = buffer.readInt16LE(16);
        var nbLine = buffer.readInt16LE(18);
        var sizeChar = buffer.readInt16LE(20);
        var sizeCol = buffer.readInt16LE(22);

        console.log("version:", version);
        console.log("nbCol:", nbCol);
        console.log("nbLine:", nbLine);
        console.log("sizeChar:", sizeChar);
        console.log("sizeCol:", sizeCol);

        var bitmap = new Bitmap(nbCol, nbLine, sizeCol, buffer.slice(24, buffer.length));
        bitmap.display();
    });
});